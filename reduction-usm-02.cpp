#include <CL/sycl.hpp>
#include <iostream>
#include <array>
#include <chrono>

using namespace cl::sycl;
//using namespace sycl::ONEAPI;

#include <cassert>
template<class T>
bool almost_equal(T x, T y, double tol)
{
    return std::abs( x - y ) < tol ;
}

typedef std::chrono::system_clock Clock;

#ifndef ITERATIONS
#  define ITERATIONS 1
#endif

static unsigned int verbose=1;
static size_t warmups=1;

// SYCL lambda function kernel names
class kernel_reduction;

int main(int argc, char **argv)
{

  if(argc < 2){
    std::cerr << "Usage <vector size> <workgroup size>" << std::endl;
    exit(1);
  }

  size_t N = atoi(argv[1]);
  size_t workgroup_size = atoi(argv[2]);
  size_t num_groups = N/workgroup_size; 
  size_t iterations = ITERATIONS;

  default_selector selector;
  queue Q(selector);
 
  if (verbose > 0) {
    // Print device information
    std::cout << "Getting device info: ";
    std::cout << "Device " << selector.select_device().get_info<info::device::name>() \
	      << ": Driver " << selector.select_device().get_info<info::device::driver_version>() << std::endl;
    std::cout << "max compute units = " 
	      << Q.get_device().get_info<info::device::max_compute_units>() << "\n";
    std::cout << "max workgroup size = " 
	      << Q.get_device().get_info<info::device::max_work_group_size>() << "\n";
    std::cout << "local memory size = " 
	      << Q.get_device().get_info<info::device::local_mem_size>() << "\n";
    std::cout << std::flush;
  }

  double *input = (double*)malloc(sizeof(double)*N);
  for (size_t i = 0; i < N; i++) { input[i] = i; }

  double chksum = 0.;
  for(size_t i = 0; i < N; ++i) {
    chksum += input[i]; 
  }

  double* d_sum = malloc_shared<double>(1, Q);
  double* d_in  = malloc_device<double>(N, Q);

  Q.memcpy(d_in, input, N*sizeof(double));
  Q.wait();

  range global_range{N};
  range local_range{workgroup_size}; 

  if (verbose > 0) {
    std::cout << "Setting number of work items to " << N << std::endl;
    std::cout << "Setting number of work groups to " << num_groups << std::endl;
    std::cout << "Setting workgroup size to " <<  N/num_groups << std::endl;
  }

  auto tstart = Clock::now();
  for (size_t iters=0; iters<iterations+warmups; ++iters) {
     if (iters == warmups) {
        Q.wait();
        tstart = Clock::now();
     } 
     *d_sum = 0.;
     Q.submit([&](handler &h) {
        h.parallel_for<class kernel_reduction>(nd_range{global_range, local_range}, sycl::ONEAPI::reduction(d_sum, sycl::ONEAPI::plus<>()), [=] (nd_item<1> id, auto& d_sum) {
           auto global_id = id.get_global_id(0);
           d_sum += d_in[global_id];
        });
     });
     Q.wait();
  }
  double ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();

  if (verbose > 0)
    std::cout << "Total execution time = " << ttotal / 1.0e6 << " secs" << std::endl;
 
  assert(almost_equal<double>(*d_sum, chksum, 2E-6));

  free(input);
  free(d_sum, Q);
  free(d_in, Q);

  return 0;
}

